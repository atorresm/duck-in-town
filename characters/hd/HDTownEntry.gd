extends InteractiveObject

# HD Town Entry

func _ready() -> void:
	is_npc = true
	is_takeable = false
	object_name = "HD"
	._ready()


func talk_look() -> void:
	.talk_look()
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["postgame_started"]):
		dialog_id = "hdtownentry_duckson_postgame_1"
	elif GlobalStatus.story_flags_unlocked(["hd_challenge"]):
		dialog_id = "hdtownentry_duckson_3"
	elif GlobalStatus.story_flags_unlocked(["hdtownentry_duckson_1_finished"]):
		dialog_id = "hdtownentry_duckson_2"
	else:
		dialog_id = "hdtownentry_duckson_1"
	GUI.start_dialog(dialog_id, [self])


func give_use_object(_obj_id : String) -> void:
	.talk_look()
	var dialog_id := ""
	match _obj_id:
		"box":
			dialog_id = "hd_give_box"
		"barrel":
			dialog_id = "hd_give_barrel"
		"nachos":
			dialog_id = "hd_give_nachos"
		"trophy":
			dialog_id = "hd_give_trophy"
		"coinroyal":
			dialog_id = "hd_give_coinroyal"
		"coindarling", "coindogge", "coinduckeriv", "coindurkson", "coinfulcrum", "coinlilly":
			dialog_id = "hd_give_anycoin"
		_:
			dialog_id = "hd_give_anything"
	GUI.start_dialog(dialog_id, [self])
