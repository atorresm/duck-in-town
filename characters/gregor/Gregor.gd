extends InteractiveObject


func _ready() -> void:
	is_npc = true
	is_takeable = false
	object_name = "Gregor"
	._ready()


func talk_look() -> void:
	.talk_look()
	# Parent is Town.tscn
	#if not GlobalStatus.story_flags_unlocked(["squawk_contest_won"]):
	#	yield(get_parent().start_squawk_minigame(), "completed")
	#else:
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["postgame_started"]):
		dialog_id = "gregor_duckson_postgame_1"
	elif GlobalStatus.story_flags_unlocked(["squawk_contest_won"]):
		dialog_id = "gregor_duckson_8"
	elif GlobalStatus.story_flags_unlocked(["squawk_contest_failed"]):
		dialog_id = "gregor_duckson_7"
	elif GlobalStatus.story_flags_unlocked(["gregor_coinroyal_given_all", "lucky_coinroyal_given"]):
		dialog_id = "gregor_duckson_6"
	elif GlobalStatus.story_flags_unlocked(["gregor_coinroyal_given_all"]):
		dialog_id = "gregor_duckson_5"
	elif GlobalStatus.story_flags_unlocked(["gregor_coinroyal_given_twice"]):
		dialog_id = "gregor_duckson_4"
	elif GlobalStatus.story_flags_unlocked(["gregor_coinroyal_given"]):
		dialog_id = "gregor_duckson_3"
	elif GlobalStatus.story_flags_unlocked(["gregor_duckson_1_finished"]):
		dialog_id = "gregor_duckson_2"
	else:
		dialog_id = "gregor_duckson_1"
	GUI.start_dialog(dialog_id, [self])
	yield(GUI, "dialog_finished")
	if GlobalStatus.story_flags_unlocked(["squawk_contest_start"]):
		var squawk_contest_already_won = GlobalStatus.story_flags_unlocked(["squawk_contest_won"])
		var squawk_contest_won_now := false
		yield(get_parent().start_squawk_minigame(), "completed")
		GlobalStatus.remove_story_flag("squawk_contest_start")
		var d := ""
		if get_parent().squawk_contest_result:
			# contest won
			d = "gregor_duckson_10"
			if not squawk_contest_already_won:
				# Only add trophy if it's the first time the player wins
				squawk_contest_won_now = true
		else:
			d = "gregor_duckson_9"
		.talk_look()
		GUI.start_dialog(d, [self])
		yield(GUI, "dialog_finished")
		if squawk_contest_won_now:
			GlobalStatus.add_item("trophy")


func give_use_object(_obj_id : String) -> void:
	.talk_look()
	var dialog_id := ""
	match _obj_id:
		"trophy":
			if GlobalStatus.story_flags_unlocked(["postgame_started"]):
				dialog_id = "gregor_give_trophy_postgame"
			else:
				dialog_id = "gregor_give_trophy"
		"nachos":
			dialog_id = "gregor_give_nachos"
		"coinroyal":
			if GlobalStatus.story_flags_unlocked(["gregor_coinroyal_given_all", "lucky_coinroyal_given"]):
				dialog_id = "gregor_give_coinroyal_all_ready"
			elif GlobalStatus.story_flags_unlocked(["gregor_coinroyal_given_all"]):
				dialog_id = "gregor_duckson_5"
			elif GlobalStatus.story_flags_unlocked(["gregor_coinroyal_given_twice"]):
				dialog_id = "gregor_give_coinroyal_3"
			elif GlobalStatus.story_flags_unlocked(["gregor_coinroyal_given"]):
				dialog_id = "gregor_give_coinroyal_2"
			else:
				dialog_id = "gregor_give_coinroyal"
		"coindarling", "coindogge", "coinduckeriv", "coindurkson", "coinfulcrum", "coinlilly":
			dialog_id = "gregor_give_anycoin"
		_:
			dialog_id = "gregor_give_anything"
	GUI.start_dialog(dialog_id, [self])