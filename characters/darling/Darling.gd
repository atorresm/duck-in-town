extends InteractiveObject


func _ready() -> void:
	is_npc = true
	is_takeable = false
	object_name = "Darling"
	._ready()


func talk_look() -> void:
	.talk_look()
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["postgame_started"]):
		dialog_id = "darling_duckson_postgame_1"
	elif GlobalStatus.story_flags_unlocked(["coindarling_given", "roldan_read"]):
		dialog_id = "darling_duckson_4"
	elif GlobalStatus.story_flags_unlocked(["coindarling_given"]):
		dialog_id = "darling_duckson_3"
	elif GlobalStatus.story_flags_unlocked(["roldan_read"]):
		dialog_id = "darling_duckson_2"
	else:
		dialog_id = "darling_duckson_1"
	GUI.start_dialog(dialog_id, [self])


func give_use_object(_obj_id : String) -> void:
	.talk_look()
	var dialog_id := ""
	match _obj_id:
		"barrel":
			var barrel_count := _count_barrels_already_given()
			if barrel_count == 7:
				if GlobalStatus.story_flags_unlocked(["nachos_given"]):
					dialog_id = "darling_give_barrel_full"
				else:
					dialog_id = "darling_give_barrel_all"
			else:
				dialog_id = "darling_give_barrel"
		"nachos":
			var barrel_count := _count_barrels_already_given()
			if barrel_count == 8:
				dialog_id = "darling_give_nachos_full"
			else:
				dialog_id = "darling_give_nachos"
		"coindarling":
			dialog_id = "darling_give_coindarling"
		_:
			dialog_id = "darling_give_anything"
	GUI.start_dialog(dialog_id, [self])


func _count_barrels_already_given() -> int:
	var res := 0
	for flag in GlobalStatus.current_story_flags:
		if flag == "barrel_given":
			res += 1
	return res