extends InteractiveObject


func _ready() -> void:
	is_npc = true
	is_takeable = false
	object_name = "Lilly"
	._ready()


func talk_look() -> void:
	.talk_look()
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["coinlilly_received"]):
		dialog_id = "lilly_duckson_postgame_5"
	elif GlobalStatus.papeo_island_high_score >= 2500 and GlobalStatus.story_flags_unlocked(["lilly_duckson_postgame_2_finished"]):
		dialog_id = "lilly_duckson_postgame_4"
	elif GlobalStatus.story_flags_unlocked(["lilly_duckson_postgame_2_finished"]):
		dialog_id = "lilly_duckson_postgame_3"
	elif GlobalStatus.story_flags_unlocked(["fulcrum_duckson_postgame_2_finished"]):
		dialog_id = "lilly_duckson_postgame_2"
	elif GlobalStatus.story_flags_unlocked(["postgame_started"]):
		dialog_id = "lilly_duckson_postgame_1"
	elif GlobalStatus.story_flags_unlocked(["lilly_duckson_1_finished"]):
		dialog_id = "lilly_duckson_2"
	else:
		dialog_id = "lilly_duckson_1"
	GUI.start_dialog(dialog_id, [self])


func give_use_object(_obj_id : String) -> void:
	.talk_look()
	var dialog_id := ""
	match _obj_id:
		"barrel":
			dialog_id = "lilly_give_barrel"
		"royalcoinonastring":
			dialog_id = "lilly_give_royalcoinonastring"
		"coinroyal":
			dialog_id = "lilly_give_coinroyal"
		"coindarling":
			dialog_id = "lilly_give_coindarling"
		"coindogge":
			dialog_id = "lilly_give_coindogge"
		"coinduckeriv":
			dialog_id = "lilly_give_coinduckeriv"
		"coindurkson":
			dialog_id = "lilly_give_coindurkson"
		"coinlilly":
			dialog_id = "lilly_give_coinlilly"
		"coinfulcrum":
			if GlobalStatus.story_flags_unlocked(["coinfulcrum_given"]):
				dialog_id = "lilly_give_coinfulcrum_2"
			else:
				dialog_id = "lilly_give_coinfulcrum"
		"trophy":
			dialog_id = "lilly_give_trophy"
		_:
			dialog_id = "lilly_give_anything"
	GUI.start_dialog(dialog_id, [self])