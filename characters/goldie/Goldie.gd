extends InteractiveObject


func _ready() -> void:
	is_npc = true
	is_takeable = false
	object_name = "Goldie"
	npc_turn_on_talk = false
	._ready()


func _physics_process(delta: float) -> void:
	object_name = "Goldie" if GlobalStatus.story_flags_unlocked(["goldie_name"]) else "???"


func talk_look() -> void:
	.talk_look()
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["royalcoin_received"]):
		dialog_id = "goldie_duckson_postgame_2"
	elif GlobalStatus.story_flags_unlocked(["postgame_started"]):
		dialog_id = "goldie_duckson_postgame_1"
	elif GlobalStatus.story_flags_unlocked(["goldie_name"]):
		dialog_id = "goldie_duckson_2"
	else:
		dialog_id = "goldie_duckson_1"
	GUI.start_dialog(dialog_id, [self])

func give_use_object(_obj_id : String) -> void:
	.talk_look()
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["goldie_name"]):
		dialog_id = "goldie_give_anything_2"
	else:
		dialog_id = "goldie_give_anything"
	GUI.start_dialog(dialog_id, [self])