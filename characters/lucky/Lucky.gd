extends InteractiveObject


func _ready() -> void:
	is_npc = true
	is_takeable = false
	object_name = "Lucky"
	._ready()


func talk_look() -> void:
	.talk_look()
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["postgame_started"]):
		dialog_id = "lucky_duckson_postgame_1"
	elif GlobalStatus.story_flags_unlocked(["squawk_contest_won"]):
		dialog_id = "lucky_duckson_6"
	elif GlobalStatus.story_flags_unlocked(["lucky_coinroyal_given"]):
		dialog_id = "lucky_duckson_5"
	elif GlobalStatus.story_flags_unlocked(["lucky_duckson_1_finished", "gregor_duckson_1_finished"]):
		dialog_id = "lucky_duckson_4"
	elif GlobalStatus.story_flags_unlocked(["lucky_duckson_1_finished"]):
		dialog_id = "lucky_duckson_3"
	elif GlobalStatus.story_flags_unlocked(["gregor_duckson_1_finished"]):
		dialog_id = "lucky_duckson_2"
	else:
		dialog_id = "lucky_duckson_1"
	GUI.start_dialog(dialog_id, [self])


func give_use_object(_obj_id : String) -> void:
	.talk_look()
	var dialog_id := ""
	match _obj_id:
		"coindurkson":
			dialog_id = "lucky_give_coindurkson"
		"coinroyal":
			if GlobalStatus.story_flags_unlocked(["lucky_coinroyal_given"]):
				dialog_id = "lucky_give_coinroyal_again"
			else:
				dialog_id = "lucky_give_coinroyal"
		"nachos":
			dialog_id = "lucky_give_nachos"
		"trophy":
			dialog_id = "lucky_give_trophy"
		_:
			dialog_id = "lucky_give_anything"
	GUI.start_dialog(dialog_id, [self])