extends InteractiveObject


func _ready() -> void:
	is_npc = true
	is_takeable = false
	object_name = "Ducker III"
	._ready()


func talk_look() -> void:
	.talk_look()
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["postgame_started"]):
		dialog_id = "duckeriii_duckson_postgame_1"
	elif GlobalStatus.story_flags_unlocked(["duckeriii_duckson_1_finished"]):
		dialog_id = "duckeriii_duckson_2"
	else:
		dialog_id = "duckeriii_duckson_1"
	GUI.start_dialog(dialog_id, [self])


func give_use_object(_obj_id : String) -> void:
	.talk_look()
	var dialog_id := ""
	match _obj_id:
		"barrel":
			dialog_id = "duckeriii_give_barrel"
		"hanapicture":
			dialog_id = "duckeriii_give_hanapicture"
		"gatopicture":
			dialog_id = "duckeriii_give_gatopicture"
		"nachos":
			dialog_id = "duckeriii_give_nachos"
		"royalcoinonastring":
			GUI.start_dialog("duckeriii_give_royalcoinonastring", [self])
			yield(GUI, "dialog_finished")
			GlobalStatus.change_scene_to("res://scenes/endgame/Endgame.tscn")
			return
		"trophy":
			dialog_id = "duckeriii_give_trophy"
		"coindarling", "coinduckeriv", "coindurkson", "coinlilly", "coinfulcrum", "coindogge", "coinroyal":
			dialog_id = "duckeriii_give_anycoin"
		_:
			dialog_id = "duckeriii_give_anything"
	GUI.start_dialog(dialog_id, [self])