extends InteractiveObject


func _ready() -> void:
	is_npc = true
	is_takeable = false
	object_name = "Greenier"
	._ready()


func talk_look() -> void:
	.talk_look()
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["postgame_started"]):
		dialog_id = "greenier_duckson_postgame_1"
	elif GlobalStatus.story_flags_unlocked(["arcade_open"]):
		dialog_id = "greenier_duckson_3"
	elif GlobalStatus.story_flags_unlocked(["greenier_duckson_1_finished"]):
		dialog_id = "greenier_duckson_2"
	else:
		dialog_id = "greenier_duckson_1"
	GUI.start_dialog(dialog_id, [self])


func give_use_object(_obj_id : String) -> void:
	.talk_look()
	var dialog_id := ""
	match _obj_id:
		"box":
			dialog_id = "greenier_give_box"
			# hide Gato painting
			get_parent().hide_gato_picture()
		"barrel":
			if GlobalStatus.story_flags_unlocked(["all_barrels_picked_up"]):
				if GlobalStatus.story_flags_unlocked(["barrel_all_given"]):
					dialog_id = "greenier_give_barrel_all_2"
				else:
					dialog_id = "greenier_give_barrel_all"
			else:
				dialog_id = "greenier_give_barrel"
		"coinroyal":
			if GlobalStatus.story_flags_unlocked(["arcade_open"]):
				dialog_id = "greenier_give_coinroyal_2"
			else:
				dialog_id = "greenier_give_coinroyal"
		"gatopicture":
			dialog_id = "greenier_give_gatopicture"
		_:
			dialog_id = "greenier_give_anything"
	GUI.start_dialog(dialog_id, [self])