extends InteractiveObject


func _ready() -> void:
	is_npc = true
	is_takeable = false
	object_name = "Durkson"
	._ready()


func talk_look() -> void:
	.talk_look()
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["durkson_quest"]):
		dialog_id = "durkson_duckson_postgame_3"
	elif GlobalStatus.story_flags_unlocked(["durkson_duckson_postgame_1_finished"]):
		dialog_id = "durkson_duckson_postgame_2"
	elif GlobalStatus.story_flags_unlocked(["postgame_started"]):
		dialog_id = "durkson_duckson_postgame_1"
	elif GlobalStatus.story_flags_unlocked(["gregor_duckson_1_finished", "coindurkson_given"]):
		dialog_id = "durkson_duckson_6"
	elif GlobalStatus.story_flags_unlocked(["coindurkson_given"]):
		dialog_id = "durkson_duckson_5"
	elif GlobalStatus.story_flags_unlocked(["durkson_duckson_1_finished", "gregor_duckson_1_finished"]):
		dialog_id = "durkson_duckson_4"
	elif GlobalStatus.story_flags_unlocked(["durkson_duckson_1_finished"]) and not GlobalStatus.story_flags_unlocked(["gregor_duckson_1_finished"]):
		dialog_id = "durkson_duckson_3"
	elif GlobalStatus.story_flags_unlocked(["gregor_duckson_1_finished"]) and not GlobalStatus.story_flags_unlocked(["durkson_duckson_1_finished"]):
		dialog_id = "durkson_duckson_2"
	else:
		dialog_id = "durkson_duckson_1"
	GUI.start_dialog(dialog_id, [self])


func give_use_object(_obj_id : String) -> void:
	.talk_look()
	var dialog_id := ""
	match _obj_id:
		"trophy":
			dialog_id = "durkson_give_trophy"
		"coinroyal":
			dialog_id = "durkson_give_coinroyal"
		"coindurkson":
			dialog_id = "durkson_give_coindurkson"
		_:
			dialog_id = "durkson_give_anything"
	GUI.start_dialog(dialog_id, [self])