shader_type spatial;

uniform sampler2D albedo_tex : hint_albedo;

void fragment() {
	ALBEDO = texture(albedo_tex, UV).rgb;
}

void light() {
	vec3 light_color = ALBEDO;
	vec3 shadow_color = light_color * 0.4;
	// Add rim
	float rim = 1.0 - dot(normalize(VIEW), NORMAL);
	shadow_color += shadow_color * pow(rim, 1.0) * 1.2;
	float factor = smoothstep(0.1, 0.15, dot(NORMAL, LIGHT * ATTENUATION));
	DIFFUSE_LIGHT = mix(shadow_color, light_color, factor);
}