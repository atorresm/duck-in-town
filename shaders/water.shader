shader_type spatial;

render_mode blend_mix, depth_draw_always;

uniform float wave_length = 4.0;
uniform float wave_amplitude = 0.2;
uniform float wave_speed = 0.15;
uniform float wave_height = 1.5;
uniform float opacity = 1.0;
uniform vec4 albedo : hint_color = vec4(0.0624, 0.3485, 0.3554, 1.0);
uniform vec4 shoreline_color : hint_color = vec4(1.0, 1.0, 1.0, 1.0);
uniform vec4 absorption_color : hint_color = vec4(0.0195, 1.0, 0.5174, 1.0);
uniform float shoreline_size = 2.0;
uniform float near_plane = 0.1;
uniform float far_plane = 400.0;

float generate_offset(vec2 xz, float factor1, float factor2, float time) {
	float pi = 3.141592;
	float x = xz[0];
	float z = xz[1];
	float xval = ((mod(x + z * x * factor1, wave_length) / wave_length) + (time * wave_speed) * mod(x * 0.8 + z, 1.5)) * 2.0 * pi;
	float zval = ((mod(factor2 * (z * x + x * z), wave_length) / wave_length) + (time * wave_speed) * 2.0 * mod(x, 2.0)) * 2.0 * pi;
	return wave_amplitude * 0.5 * (sin(zval) + cos(xval));
}

vec3 apply_distortion(vec3 point, float time) {
	float x_dist = generate_offset(point.xz, 0.2, 0.1, time);
	float y_dist = generate_offset(point.xz, 0.5, 0.3, time) * wave_height;
	float z_dist = generate_offset(point.xz, 0.15, 0.2, time);
	return point + vec3(x_dist, y_dist, z_dist);
}

void vertex() {
	VERTEX = apply_distortion(VERTEX, TIME);
}

float to_linear_depth(float val) {
	float near = near_plane;
	float far = far_plane;
	val = 2.0 * val - 1.0;
	val = 2.0 * near * far / (far + near - val * (far - near));
	return val;
}

float calculate_water_depth(sampler2D depth_tex, vec2 screen_uv, float frag_coord_z) {
	float floor_distance = to_linear_depth(texture(depth_tex, screen_uv).r);
	float water_distance = to_linear_depth(frag_coord_z);
	return floor_distance - water_distance;
}

void fragment() {
	// Low-poly
	NORMAL = normalize(cross(dFdx(VERTEX), dFdy(VERTEX)));
  
	// Water color
	ALBEDO = albedo.rgb;
	ROUGHNESS = 0.1;
	SPECULAR = 0.2;
  
	// Transparency
	ALPHA = opacity;
  
	// Shore and light absorption
	float water_depth = calculate_water_depth(DEPTH_TEXTURE, SCREEN_UV, FRAGCOORD.z);
	if (water_depth < shoreline_size) {
		ALBEDO = shoreline_color.rgb;
	} else if (water_depth < 10.0) {
		float factor = smoothstep(1.0, 0.0, water_depth / 1.75);
		ALBEDO = absorption_color.rgb * factor + albedo.rgb;
	}
}