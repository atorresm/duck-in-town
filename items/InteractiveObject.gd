extends Spatial
class_name InteractiveObject
# This interface must be implemented by items/NPCs

var is_npc : bool
var is_takeable : bool
var is_portal : bool
var can_receive_object : bool = true
var can_be_talked_looked : bool = true
var npc_turn_on_talk : bool = true # wether turn to talk to Duckson or not
var object_name : String
var item_id : String # only relevant if not is_npc and  is_takeable

# Optional custom camera transform for conversations
export(NodePath) var custom_talk_cam_transform_spatial = null
# Internal parameters for custom camera transform interpolation
var active_cam_transform_before_talk : Transform = Transform()
var rotation_degrees_before_talk : Vector3 = Vector3()
var anim_player : AnimationPlayer = null


func _ready() -> void:
	rotation_degrees_before_talk = self.rotation_degrees
	anim_player = find_node("AnimationPlayer")
	if anim_player != null and is_npc:
		anim_player.play("idle-loop")


# Implement if not is_portal
func talk_look() -> void:
	change_to_custom_cam_transform()
	if is_npc:
		rotate_towards_player()

# Implement if not is_portal
func give_use_object(_obj_id : String) -> void:
	# When overriden, this dialog won't play
	change_to_custom_cam_transform()
	if is_npc:
		rotate_towards_player()
	GUI.start_dialog("duckson_generic_give_item", [self])


# Implement only if is_portal
func go_to() -> void:
	pass


# Implement only if not is_npc and not is_portal and is_takeable
func take() -> void:
	assert(not is_npc and not is_portal)
	if not is_takeable:
		return
	is_takeable = false # only take once
	GlobalStatus.add_item(item_id)


func _current_dialog_char_name_changed(char_name : String) -> void:
	if not is_npc:
		return # doesn't have talk anim
	
	if anim_player == null:
		return
	
	if char_name == object_name: 
		if not anim_player.current_animation == "Talk-loop":
			anim_player.play("Talk-loop")
	else:
		if not anim_player.current_animation == "idle-loop":
			anim_player.play("idle-loop")


func change_to_custom_cam_transform() -> void:
	if custom_talk_cam_transform_spatial == null:
		return
	
	var current_cam : Camera = get_viewport().get_camera()
	# Only set the first time to assure the neutral position
	if active_cam_transform_before_talk == Transform():
		active_cam_transform_before_talk = current_cam.global_transform
	
	# Interpolate position
	var initial_pos = current_cam.global_transform.origin
	var target_pos := (get_node(custom_talk_cam_transform_spatial) as Spatial).global_transform.origin
#warning-ignore:return_value_discarded
	GlobalStatus.tween.interpolate_property(current_cam, "global_transform:origin", initial_pos, target_pos, 0.8, Tween.TRANS_QUART, Tween.EASE_IN_OUT)
	
	var initial_basis = current_cam.global_transform.basis
	var target_basis := (get_node(custom_talk_cam_transform_spatial) as Spatial).global_transform.basis
#warning-ignore:return_value_discarded
	GlobalStatus.tween.interpolate_property(current_cam, "global_transform:basis", initial_basis, target_basis, 0.675, Tween.TRANS_QUAD, Tween.EASE_IN)
	
#warning-ignore:return_value_discarded
	GlobalStatus.tween.start()
	yield(GlobalStatus.tween, "tween_completed")


func change_to_previous_active_cam_transform() -> void:
	if active_cam_transform_before_talk == Transform():
		return
	
	var current_cam : Camera = get_viewport().get_camera()
	var initial_pos = current_cam.global_transform.origin
	var target_pos := active_cam_transform_before_talk.origin
#warning-ignore:return_value_discarded
	GlobalStatus.tween.interpolate_property(current_cam, "global_transform:origin", initial_pos, target_pos, 0.8, Tween.TRANS_QUART, Tween.EASE_IN_OUT)
	
	var initial_basis = current_cam.global_transform.basis
	var target_basis := active_cam_transform_before_talk.basis
#warning-ignore:return_value_discarded
	GlobalStatus.tween.interpolate_property(current_cam, "global_transform:basis", initial_basis, target_basis, 0.8, Tween.TRANS_QUAD, Tween.EASE_OUT)
#warning-ignore:return_value_discarded
	GlobalStatus.tween.start()


func rotate_towards_player() -> void:
	rotation_degrees_before_talk = self.rotation_degrees
	var player_pos : Vector3 = GlobalStatus.player.global_transform.origin
	var self_pos : Vector3 = self.global_transform.origin
	var dir = player_pos - self_pos
	var angle := rad2deg(atan2(dir.x, dir.z))
	var degs_to_rot = int(angle - rotation_degrees.y + 360) % 360
	if degs_to_rot > 180:
		# Choose dir to rotate
		degs_to_rot = -(360 - degs_to_rot)
	if npc_turn_on_talk:
		GlobalStatus.tween.interpolate_property(self, "rotation_degrees:y", rotation_degrees.y, rotation_degrees.y + degs_to_rot, 0.15, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		GlobalStatus.tween.start()


func rotate_player_towards_object() -> void:
	var player_pos : Vector3 = GlobalStatus.player.global_transform.origin
	var self_pos : Vector3 = self.global_transform.origin
	var dir = self_pos - player_pos
	var angle := rad2deg(atan2(dir.x, dir.z))
	var degs_to_rot = int(angle - GlobalStatus.player.rotation_degrees.y + 360) % 360
	if degs_to_rot > 180:
		# Choose dir to rotate
		degs_to_rot = -(360 - degs_to_rot)
	GlobalStatus.tween.interpolate_property(GlobalStatus.player, "rotation_degrees:y", GlobalStatus.player.rotation_degrees.y, GlobalStatus.player.rotation_degrees.y + degs_to_rot, 0.15, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	GlobalStatus.tween.start()


func restore_rotation() -> void:
	GlobalStatus.tween.interpolate_property(self, "rotation_degrees", rotation_degrees, rotation_degrees_before_talk, 0.15, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	GlobalStatus.tween.start()


func _on_dialog_finished() -> void:
	change_to_previous_active_cam_transform()
	restore_rotation()
	if anim_player != null and not anim_player.current_animation == "idle-loop":
		anim_player.play("idle-loop")
