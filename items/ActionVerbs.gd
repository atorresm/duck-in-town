class_name ActionVerbs

const TALK_LOOK := "talk_look" # Talk to NPC / Look object
const GIVE_USE_OBJECT := "give_use_object" # Give object to NPC / Use object on object
const TAKE := "take" # Take object
const GO_TO := "go_to" # Go to x place...
