extends Node

const ITEMS_JSON_PATH := "res://assets/items/items.json"
var items : Dictionary = {}

func _ready() -> void:
	_parse_items()


func _parse_items() -> void:
	var f := File.new()
	var f_error := f.open(ITEMS_JSON_PATH, f.READ)
	
	if f_error != OK:
		printerr("There was an error opening the items json.")
		return
	
	var json := f.get_as_text()
	var parse_result := JSON.parse(json)
	
	if parse_result.error != OK:
		printerr("There was an error parsing the items json.")
		return
	
	items = parse_result.result


func get_item_by_id(id : String) -> Item:
	if not items.keys().has(id):
		printerr("Error getting item with id: ", id)
		return null
	
	var item := Item.new()
	var info : Dictionary = items.get(id)
	item.id = id
	item.name = info["name"]
	item.description = info["description"]
	item.icon_path = "res://assets/items/" + item.id + ".png"
	
	return item
