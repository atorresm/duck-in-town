extends Control


func _ready() -> void:
	GlobalStatus.play_scene_music("res://assets/music/mainmenu.ogg", -5)


func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	get_tree().change_scene("res://gui/menus/main/MainMenu.tscn")
