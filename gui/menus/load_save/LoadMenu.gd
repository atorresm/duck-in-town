extends MarginContainer

onready var entries := $MarginContainer/VBoxContainer/CustomScrollContainer/Entries as VBoxContainer
onready var scroll_container := $MarginContainer/VBoxContainer/CustomScrollContainer as ScrollContainer
onready var fade_anim := $Fade/Fade/AnimationPlayer as AnimationPlayer
export(PackedScene) var savedata_entry_scn = null
# Screenshot from before opening the menu. If this is not null, the menu is in "Save mode"
var screenshot_data = null

# True when this menu is handled from another scene
# (for example, from MainMenu)
var menu_controlled_externally = false

func _ready() -> void:
	if savedata_entry_scn == null:
		printerr("Savedata entry scn not set.")
		return
	
	for i in range(1, 100):
		var entry : Control = savedata_entry_scn.instance()
		entries.add_child(entry)
		# Connect entries to CustomScrollContainer, needed for following
		entry.connect("focus_entered", scroll_container, "_on_child_gained_focus", [entry])
		
		# If we are in load mode, make sure to unpause the game after selecting save
		if screenshot_data == null:
			entry.connect("slot_to_load_selected", get_tree(), "set", ["paused", false])
			# Destroy LoadMenu when save slot has been selected to load
			entry.connect("slot_to_load_selected", self, "_on_slot_to_load_selected")
		
		entry.screenshot_data = screenshot_data
		entry.savedata_slot = i
	
	entries.get_child(0).grab_focus()


func _exit_tree() -> void:
	if get_tree().paused:
		get_tree().paused = false


func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_cancel") and not menu_controlled_externally:
		queue_free()


func _on_slot_to_load_selected() -> void:
	fade_anim.play("fade_out")
	yield(fade_anim, "animation_finished")
	queue_free()
