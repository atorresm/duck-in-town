extends Control


onready var load_button := $Main/Buttons/Load as Button
export(PackedScene) var load_menu_scn = null
export(AudioStream) var main_menu_music = null
var _load_menu = null


func _ready() -> void:
	if load_menu_scn == null:
		printerr("Load menu scene not set in MainMenu.tscn.")
	if main_menu_music == null:
		printerr("Main menu music null at MainMenu.tscn")
	
	GlobalStatus.play_scene_music(main_menu_music.resource_path, -5)
	
	# For some reason starting from the SplashScreen to here
	# using the custom scene switching method crashes, so we
	# start using it from here
	GlobalStatus.current_scene = self
	$Fade/Fade/AnimationPlayer.play("fade_in")
	yield($Fade/Fade/AnimationPlayer, "animation_finished")
	$Main/Buttons.get_child(0).grab_focus()
	
	# Setup settings UI
	$Settings/MSAA/OptionButton.selected = SettingsManager.msaa
	$Settings/ShadowsQuality/OptionButton.selected = SettingsManager.directional_shadows_quality
	$Settings/Fullscreen/CheckBox.pressed = SettingsManager.fullscreen
	$Settings/Vsync/CheckBox.pressed = SettingsManager.vsync
	if SettingsManager.locale.begins_with("es"):
		$Settings/Language/OptionButton.selected = 1 # Español
	else:
		$Settings/Language/OptionButton.selected = 0 # English
	
	# Enable Load button only if there are savegames
	var dir := Directory.new()
	if dir.dir_exists("user://saves"):
		dir.open("user://saves")
		var files := []
		dir.list_dir_begin()
		var f = dir.get_next()
		while f != "":
			if f != ".." and f != ".":
				files.append(f)
			f = dir.get_next()
		load_button.disabled = files.size() == 0
	else:
		load_button.disabled = true


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_cancel") and is_instance_valid(_load_menu):
		accept_event()
		# ESC pressed and load menu is showing, delete it and show MainMenu again
		$Fade/Fade/AnimationPlayer.play("fade_out")
		yield($Fade/Fade/AnimationPlayer, "animation_finished")
		_load_menu.queue_free()
		_load_menu = null
		$Main.show()
		$Fade/Fade/AnimationPlayer.play("fade_in")
		yield($Fade/Fade/AnimationPlayer, "animation_finished")
		$Main/Buttons/Load.grab_focus()
	if event.is_action_pressed("ui_cancel") and not $Main.visible:
		_on_Back_pressed()

func _on_NewGame_pressed() -> void:
	# Disable button to avoid crash by repeteadly pressing the button while the scene is loading
	$Main/Buttons/NewGame.disabled = true
	GlobalStatus.owned_items = []
	GlobalStatus.current_story_flags = [""]
	GlobalStatus.slot_loaded_from = null
	GlobalStatus.change_scene_to("res://scenes/forest/Forest.tscn")


func _on_Exit_pressed() -> void:
	get_tree().quit()


func _on_Settings_pressed() -> void:
	$Main.hide()
	$Settings.show()
	$Settings/Language/OptionButton.grab_focus()


func _on_Help_pressed() -> void:
	$Main.hide()
	$Help.show()
	$Help/Buttons/Back.grab_focus()


func _on_Back_pressed() -> void:
	$Settings.hide()
	$Help.hide()
	$Credits.hide()
	$Main.show()
	$Main/Buttons/Load.grab_focus()


func _on_Apply_pressed() -> void:
	var msaa_idx : int = $Settings/MSAA/OptionButton.selected
	var shadows_idx : int = $Settings/ShadowsQuality/OptionButton.selected
	var fullscreen : bool = $Settings/Fullscreen/CheckBox.pressed
	var vsync : bool = $Settings/Vsync/CheckBox.pressed
	var locale : String = "es" if $Settings/Language/OptionButton.selected == 1 else "en"
	
	SettingsManager.fullscreen = fullscreen
	SettingsManager.msaa = msaa_idx
	SettingsManager.vsync = vsync
	SettingsManager.directional_shadows_quality = shadows_idx
	SettingsManager.locale = locale
	SettingsManager.apply_settings()


func _on_Load_pressed() -> void:
	$Fade/Fade/AnimationPlayer.play("fade_out")
	yield($Fade/Fade/AnimationPlayer, "animation_finished")
	var lm = load_menu_scn.instance()
	lm.menu_controlled_externally = true
	$Main.hide()
	add_child(lm)
	$Fade/Fade/AnimationPlayer.play("fade_in")
	yield($Fade/Fade/AnimationPlayer, "animation_finished")
	_load_menu = lm


func _on_DefaultSettings_pressed() -> void:
	var defs = SettingsManager.get_default_settings()
	$Settings/MSAA/OptionButton.selected = defs["msaa"]
	$Settings/ShadowsQuality/OptionButton.selected = defs["directional_shadows_quality"]
	$Settings/Fullscreen/CheckBox.pressed = defs["fullscreen"]
	$Settings/Vsync/CheckBox.pressed = defs["vsync"]
	if defs["locale"].begins_with("es"):
		$Settings/Language/OptionButton.select(1)
	else:
		$Settings/Language/OptionButton.select(0)


func _on_Credits_pressed() -> void:
	$Main.hide()
	$Credits.show()
	$Credits/Buttons/Back.grab_focus()
