extends ScrollContainer
class_name CustomScrollContainer
# Scroll container that follows its focused child
# childs must connect their "focus_entered" signal to 
# "_on_child_gained_focus" method

onready var tween := $Tween as Tween
export(bool) var show_scrollbars := true


func _ready() -> void:
	if not show_scrollbars:
		# Please tell me if there is any better way to do this
		var empty_style := StyleBoxEmpty.new()
		for bar in [get_h_scrollbar(), get_v_scrollbar()]:
			bar.set("custom_styles/scroll", empty_style)
			bar.set("custom_styles/scroll_focus", empty_style)
			bar.set("custom_styles/grabber", empty_style)
			bar.set("custom_styles/grabber_pressed", empty_style)
			bar.set("custom_styles/grabber_highlight", empty_style)


func _on_child_gained_focus(focus_owner : Control) -> void:
	var focus_offset = focus_owner.rect_position.y

	var scrolled_top = scroll_vertical
	var scrolled_bottom = scrolled_top + rect_size.y - focus_owner.rect_size.y
	
	if focus_offset < scrolled_top:
		# Scroll up
		# Makes the interpolation so the focus_owner is displayed on top of the screen
		tween.interpolate_property(self, "scroll_vertical", scroll_vertical, \
			focus_offset, 0.2, Tween.TRANS_EXPO, Tween.EASE_OUT)
		tween.start()
	elif focus_offset >= scrolled_bottom:
		# Scroll down
		# Makes the interpolation so the focus_owner is displayed the last on 
		# the screen
		tween.interpolate_property(self, "scroll_vertical", scroll_vertical, \
			focus_offset - rect_size.y + focus_owner.rect_size.y, 0.2, Tween.TRANS_EXPO, Tween.EASE_OUT)
		tween.start()