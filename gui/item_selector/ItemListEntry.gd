extends TextureButton

var item : Item setget _set_item

func _set_item(i : Item) -> void:
	texture_normal = load(i.icon_path)
	item = i