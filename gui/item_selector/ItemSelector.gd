extends MarginContainer

const ITEMS_PER_PAGE := 24
export(PackedScene) var item_list_entry_scn
onready var pages := $MarginContainer/VBoxContainer/Pages as MarginContainer
onready var tween := $MarginContainer/Tween as Tween
onready var indicator := $MarginContainer/Control/Indicator as TextureRect
onready var item_name := $MarginContainer/VBoxContainer/VBoxContainer/ItemName
onready var item_descr := $MarginContainer/VBoxContainer/VBoxContainer/Description
onready var empty_label := $MarginContainer/EmptyLabel as Label
var selected_item : Item = null
var entries := []

signal item_selected

func _ready() -> void:
	# Don't allow player movement when this menu is shown
	get_tree().paused = true
	self.hide()
	# Keep in mind pagination is not fully implemented:
	# page indicators and page switching is not done
	_fill_pages()
	self.show()


func _exit_tree() -> void:
	get_tree().paused = false


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_cancel"):
		# The item selected is null, but we emit the signal
		# to notify the GUI singleton that no item was selected
		emit_signal("item_selected")


func _fill_pages() -> void:
	indicator.hide()
	item_name.hide()
	item_descr.hide()
	
	if GlobalStatus.owned_items.size() == 0:
		empty_label.show()
		return
	
	for id in GlobalStatus.owned_items:
		var item : Item = ItemDB.get_item_by_id(id)
		var entry = item_list_entry_scn.instance()
		entry.item = item
		
		# create a new page if needed and add entry there
		if pages.get_child(pages.get_child_count() - 1).get_child_count() == ITEMS_PER_PAGE:
			var new_page := GridContainer.new()
			new_page.columns = 6
			new_page.size_flags_horizontal = Control.SIZE_SHRINK_CENTER
			new_page.size_flags_vertical = Control.SIZE_SHRINK_CENTER
			pages.add_child(new_page)
		
		var current_page := pages.get_child(pages.get_child_count() - 1)
		current_page.add_child(entry)
		entry.connect("focus_entered", self, "_on_item_list_entry_focus_entered", [entry])
		entry.connect("pressed", self, "_on_item_list_entry_pressed", [entry])
		entries.append(entry)
	
	# Make only first page visible
	for i in range(pages.get_child_count()):
		if i != 0:
			pages.get_child(i).hide()

	# Wait a frame to make sure every child has its position assigned
	yield(get_tree(), "idle_frame")
	entries[0].grab_focus()


func _on_item_list_entry_focus_entered(entry : Control) -> void:
	item_name.show()
	item_descr.show()
	item_name.text = entry.item.name
	item_descr.text = entry.item.description
	yield(get_tree(), "idle_frame")
	var initial_pos := indicator.rect_global_position
	var final_pos := (entry.rect_global_position + entry.rect_size / 2) - indicator.rect_size / 2
	
	if not indicator.visible:
		# skip interpolation
		indicator.rect_global_position = final_pos
		indicator.show()
	else:
# warning-ignore:return_value_discarded
		tween.interpolate_property(indicator, "rect_global_position", initial_pos, final_pos, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	# warning-ignore:return_value_discarded
		tween.start()


func _on_item_list_entry_pressed(entry : Control) -> void:
	selected_item = entry.item
	emit_signal("item_selected")