extends Button

var verb := ""
var target : InteractiveObject

func set_action(action : Array):
	verb = action[0]
	target = action[1]
	
	match verb:
		ActionVerbs.TALK_LOOK:
			if target.is_npc:
				text = "· " + tr("ACTION_VERB_TALK") + " " + target.object_name
			else:
				text = "· " + tr("ACTION_VERB_LOOK") + " " + target.object_name
		ActionVerbs.GIVE_USE_OBJECT:
			if target.is_npc:
				text = "· " + tr("ACTION_VERB_GIVE_ITEM") + " " + target.object_name
			else:
				text = "· " + tr("ACTION_VERB_USE_ITEM") + " " + target.object_name
		ActionVerbs.TAKE:
			text = "· " + tr("ACTION_VERB_TAKE") + " " + target.object_name
		ActionVerbs.GO_TO:
			text = "· " + tr("ACTION_VERB_GO_TO") + " " + target.object_name


func _on_ActionListEntry_pressed() -> void:
	match verb:
		ActionVerbs.TALK_LOOK:
			target.talk_look()
		ActionVerbs.GIVE_USE_OBJECT:
			var item = yield(GUI.select_item(), "completed")
			if item != null:
				target.give_use_object(item.id)
		ActionVerbs.TAKE:
			target.take()
		ActionVerbs.GO_TO:
			# Disable button to avoid crash by repeteadly pressing the button while the scene is loading
			self.disabled = true
			target.go_to()


func _on_ActionListEntry_visibility_changed() -> void:
	if not visible and has_focus():
		release_focus()
