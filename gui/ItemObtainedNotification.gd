extends VBoxContainer

onready var anim_player := $AnimationPlayer
onready var item_icon := $ItemIcon
onready var label := $Label
var item_id : String = ""


func _ready() -> void:
	var item : Item = ItemDB.get_item_by_id(item_id)
	if item == null:
		queue_free()
		return
	
	GlobalStatus.play_sound("res://assets/music/item_get.wav", -10)
	item_icon.texture = load(item.icon_path)
	label.text = tr("ITEM_GET_LABEL") + item.name
	anim_player.play("show")
	yield(anim_player, "animation_finished")
	queue_free()