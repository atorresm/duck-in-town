extends Spatial


onready var anim_player := $AnimationPlayer as AnimationPlayer

func _ready() -> void:
	GlobalStatus.stop_all_sounds()
	anim_player.play("show_text")
	yield(anim_player, "animation_finished")
	anim_player.play("blink_next")


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_accept") and anim_player.current_animation == "blink_next":
		# Go to Town
		GlobalStatus.change_scene_to("res://scenes/town/Town.tscn")