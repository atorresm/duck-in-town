extends Light


func _physics_process(delta : float) -> void:
	shadow_enabled = SettingsManager.directional_shadows_quality != 0
