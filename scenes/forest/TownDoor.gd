extends InteractiveObject


func _ready() -> void:
	is_npc = true
	is_takeable = false
	is_portal = false
	npc_turn_on_talk = false
	object_name = tr("THE_OTHER_SIDE_LABEL")


func talk_look() -> void:
	.talk_look()
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["door_puzzle_solved"]):
		dialog_id = "guard_duckson_4"
	elif GlobalStatus.story_flags_unlocked(["duck_duckson_sesame_finished", "forest_post_read"]):
		dialog_id = "guard_duckson_3"
	elif GlobalStatus.story_flags_unlocked(["forest_post_read"]):
		dialog_id = "guard_duckson_2"
	else:
		dialog_id = "guard_duckson_1"
	GUI.start_dialog(dialog_id, [self])
