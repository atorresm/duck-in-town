extends Spatial

export(AudioStream) var endgame_music = null
onready var anim_player := $AnimationPlayer as AnimationPlayer
signal text_read

func _ready() -> void:
	if endgame_music == null:
		printerr("Endgame music null at Endgame.tscn")
		return
	
	GlobalStatus.stop_all_sounds()
	anim_player.play("show_text")
	yield(anim_player, "animation_finished")
	anim_player.play("blink_next")
	yield(self, "text_read")
	anim_player.play("hide_text")
	yield(anim_player, "animation_finished")
	
	GlobalStatus.play_scene_music(endgame_music.resource_path, -13)
	GUI.start_dialog("endgame_dialog", [])
	yield(GUI, "dialog_finished")
	GlobalStatus.add_story_flag("postgame_started")
	GlobalStatus.add_story_flag("appear_near_statue_on_postgame")
	GlobalStatus.remove_item("nachos") # You gave them to Darling...
		# Transform hardcoded after setting it up in editor
	var duckson_transform : Transform = Transform(\
			Vector3(1, 0, 0),
			Vector3(0, 1, 0),
			Vector3(0, 0, 1),
			Vector3(40, 0, -3))
	var changes := {
		"Duckson" : {
			"global_transform" : duckson_transform
		}
	}
	GlobalStatus.change_scene_to("res://scenes/town/Town.tscn", true, changes)


func _input(event: InputEvent) -> void:
	if event.is_action("ui_accept") and anim_player.current_animation == "blink_next":
		emit_signal("text_read")