extends InteractiveObject

func _ready() -> void:
	is_npc = false
	is_takeable = false
	is_portal = true
	object_name = "Town"


func go_to() -> void:
	# Transform hardcoded after setting it up in editor
	var transform_in_front_of_door : Transform = Transform(\
		Vector3(1, 0, 0),\
		Vector3(0, 1, 0),\
		Vector3(0, 0, 1),\
		Vector3(156.73, 0, -40.117)\
	)
	var changes := {
		"Duckson" : {
			"global_transform" : transform_in_front_of_door
		}
	}
	GlobalStatus.change_scene_to("res://scenes/town/Town.tscn", true, changes)
