extends InteractiveObject

func _ready() -> void:
	is_npc = false
	is_takeable = false
	is_portal = true
	object_name = "Town"


func go_to() -> void:
	if GlobalStatus.story_flags_unlocked(["barrel_all_given"]) and not GlobalStatus.story_flags_unlocked(["arcade_open"]):
		GlobalStatus.add_story_flag("arcade_open")
	
	# Transform hardcoded after setting it up in editor
	var transform_in_front_of_door : Transform = Transform(\
		Vector3(0.249, 0, 0.968),\
		Vector3(0, 1, 0),\
		Vector3(-0.968, 0, 0.249),\
		Vector3(63.744, 0, -58.503)\
	)
	var changes := {
		"Duckson" : {
			"global_transform" : transform_in_front_of_door
		}
	}
	GlobalStatus.change_scene_to("res://scenes/town/Town.tscn", true, changes)