extends Spatial

export(AudioStream) var crumbs_music = null

func _ready() -> void:
	if crumbs_music == null:
		printerr("Crumb's music null at Crumbs.tscn")
	else:
		GlobalStatus.play_scene_music(crumbs_music.resource_path, -5)
	
	if not GlobalStatus.story_flags_unlocked(["arcade_open"]):
		$Model/Post.hide()
		$Model/Post/Post/shape.disabled = true
		$ArcadePostInteractArea.monitoring = false
		$ArcadePostInteractArea.monitorable = false
	
	if GlobalStatus.story_flags_unlocked(["gato_paint_received_from_greenier"]):
		$Model/GatoPicture.hide()
	else:
		$Model/GatoPicture.show()
	
	if GlobalStatus.story_flags_unlocked(["postgame_started"]):
		$Model/HanaPicture.show()
	else:
		$Model/HanaPicture.hide()


func hide_gato_picture() -> void:
	$Model/GatoPicture.hide()