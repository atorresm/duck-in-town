extends InteractiveObject

export(NodePath) var barrel_node_path = null

func _ready() -> void:
	is_npc = false
	is_takeable = true
	is_portal = false
	can_be_talked_looked = false
	can_receive_object = false
	object_name = tr("BARREL_NAME_LABEL")
	item_id = "barrel"
	if barrel_node_path == null:
		printerr("Barrel node path is null at Barrel.gd at CrumbsArcade.tscn")
	
	get_parent().connect("hide_barrels", self, "hide_barrel")
	if not GlobalStatus.story_flags_unlocked(["greenier_requested_barrel_pickup"]):
		self.hide()
		self.monitorable = false
	
	if GlobalStatus.story_flags_unlocked([get_node(barrel_node_path).name + "_taken"]):
		hide_barrel()


func take():
	.take()
	GlobalStatus.player.play_grab_anim()
	yield(GlobalStatus.player, "grab_anim_finished")
	GlobalStatus.add_story_flag(get_node(barrel_node_path).name + "_taken")
	hide_barrel()


func hide_barrel() -> void:
	self.hide()
	self.monitorable = false
	self.monitoring = false
	var barrel = get_node(barrel_node_path)
	barrel.hide()
	var shape = barrel.find_node("shape")
	if shape != null and shape is CollisionShape:
		shape.disabled = true