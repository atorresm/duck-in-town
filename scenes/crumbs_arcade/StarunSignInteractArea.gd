extends InteractiveObject


func _ready() -> void:
	is_npc = false
	is_takeable = false
	is_portal = false
	can_receive_object = false
	object_name = tr("STARUN_SIGN_NAME_LABEL")


func talk_look():
	.talk_look()
	GUI.start_dialog("starun_sign", [self])