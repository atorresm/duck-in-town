extends InteractiveObject

func _ready() -> void:
	is_npc = false
	is_takeable = false
	is_portal = false
	object_name = tr("ARCADE_MACHINE_NAME_LABEL")


func talk_look() -> void:
	.talk_look()
	GUI.start_dialog("arcade_machine", [self])


func give_use_object(_obj_id : String) -> void:
	.talk_look()
	match _obj_id:
		"coinroyal":
			GUI.start_dialog("arcade_machine_give_coinroyal", [self])
		"royalcoinonastring":
			get_parent().play_papeo_island()
		_:
			GUI.start_dialog("duckson_generic_give_item", [self])