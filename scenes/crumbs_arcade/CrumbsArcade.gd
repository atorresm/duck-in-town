extends Spatial

onready var arcade_play_cam_pos := $ArcadePlayCameraPos as Camera
onready var cam := $Model/Camera as Camera
onready var cam_xform_before_arcade_play := cam.global_transform as Transform
onready var tween := $Tween as Tween
onready var barrels := [$Model/StrawberryBarrel1, $Model/StrawberryBarrel2,
	$Model/StrawberryBarrel3, $Model/StrawberryBarrel4, $Model/StrawberryBarrel5,
	$Model/StrawberryBarrel6, $Model/StrawberryBarrel7, $Model/StrawberryBarrel8]
export(PackedScene) var papeo_island_start_scene = null
export(AudioStream) var crumbs_arcade_music = null
export(AudioStream) var crumbs_music = null

signal hide_barrels

func _ready() -> void:
	if papeo_island_start_scene == null:
		printerr("Papeo Island start scn is null at CrumbsArcade.tscn")
	
	if GlobalStatus.story_flags_unlocked(["arcade_open"]):
		$ArcadeLight.show()
		$WarehouseLight.hide()
		$Model/Arcade.show()
		$Model/Arcade/Arcade/shape.disabled = false
		$Model/PosterRocket.show()
		$Model/PosterStarun.show()
		emit_signal("hide_barrels")
		$Model/ZombiStatue.show()
		$Model/ZombiStatue/ZombiStatue/shape.disabled = false
		if crumbs_arcade_music == null:
			printerr("Crumbs arcade music null at CrumbsArcade.tscn")
		else:
			GlobalStatus.play_scene_music(crumbs_arcade_music.resource_path, -5)
	else:
		$Model/Arcade.hide()
		$Model/Arcade/Arcade/shape.disabled = true
		$ArcadeMachineInteractArea.monitorable = false
		$Model/PosterRocket.hide()
		$Model/PosterStarun.hide()
		$Model/ZombiStatue.hide()
		$Model/ZombiStatue/ZombiStatue/shape.disabled = true
		$ZombieStatueInteractArea.monitoring = false
		$ZombieStatueInteractArea.monitorable = false
		$StarunSignInteractArea.monitoring = false
		$StarunSignInteractArea.monitorable = false
		$RocketGuardianSignInteractArea.monitoring = false
		$RocketGuardianSignInteractArea.monitorable = false
		if crumbs_music == null:
			printerr("Crumbs music null at Crumbs.tscn")
		else:
			GlobalStatus.play_scene_music(crumbs_music.resource_path, -5)
	
	if GlobalStatus.story_flags_unlocked(["all_barrels_picked_up"]):
		emit_signal("hide_barrels")


func _physics_process(delta: float) -> void:
	if GlobalStatus.story_flags_unlocked(["all_barrels_picked_up"]):
		return
	
	var all_barrels_hidden := true
	for barrel in barrels:
		all_barrels_hidden = all_barrels_hidden and not barrel.visible
	if all_barrels_hidden:
		GlobalStatus.add_story_flag("all_barrels_picked_up")


func play_papeo_island() -> void:
	tween.interpolate_property(cam, "global_transform", cam.global_transform, arcade_play_cam_pos.global_transform, 0.8, Tween.TRANS_QUART, Tween.EASE_IN_OUT)
	tween.start()
	yield(tween, "tween_completed")
	GlobalStatus.stop_all_sounds()
	GlobalStatus.change_scene_to(papeo_island_start_scene.resource_path, -10)