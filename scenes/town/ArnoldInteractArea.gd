extends InteractiveObject


func _ready() -> void:
	is_npc = true
	is_takeable = false
	is_portal = false
	object_name = "Arnold"


func talk_look():
	.talk_look()
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["arnold_quest"]):
		dialog_id = "arnold_duckson_postgame_1"
	else:
		dialog_id = "arnold_duckson_1"
	GUI.start_dialog(dialog_id, [self])


func give_use_object(_obj_id : String) -> void:
	.talk_look()
	GUI.start_dialog("arnold_give_anything", [self])