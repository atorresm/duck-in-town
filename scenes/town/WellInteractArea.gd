extends InteractiveObject


func _ready() -> void:
	is_npc = false
	is_takeable = false
	is_portal = false
	object_name = tr("WELL_NAME_LABEL")


func talk_look():
	.talk_look()
	GUI.start_dialog("well_duckson_1", [self])


func give_use_object(_obj_id : String) -> void:
	.talk_look()
	var dialog_id := ""
	match _obj_id:
		"coinroyal":
			if GlobalStatus.story_flags_unlocked(["well_coinroyal_given_again"]):
				dialog_id = "well_give_coinroyal_again_2"
			elif GlobalStatus.story_flags_unlocked(["well_coinroyal_given"]):
				dialog_id = "well_give_coinroyal_again"
			else:
				dialog_id = "well_give_coinroyal"
		"coindarling", "coindogge", "coinduckeriv", "coindurkson", "coinfulcrum", "coinlilly":
			dialog_id = "well_give_anycoin"
		_:
			dialog_id = "well_give_anything"
	GUI.start_dialog(dialog_id, [self])