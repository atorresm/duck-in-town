extends InteractiveObject


func _ready() -> void:
	is_npc = false
	is_takeable = false
	is_portal = false
	object_name = tr("STATUE_NAME_LABEL")


func talk_look():
	.talk_look()
	GUI.start_dialog("karate_statue", [self])