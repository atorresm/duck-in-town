extends InteractiveObject


func _ready() -> void:
	is_npc = false
	is_takeable = false
	is_portal = false
	object_name = tr("HOTEL_DOOR_NAME_LABEL")


func talk_look():
	.talk_look()
	if GlobalStatus.story_flags_unlocked(["postgame_started"]):
		GUI.start_dialog("hotel_small_door_postgame", [self])
	else:
		GUI.start_dialog("hotel_small_door", [self])