
extends Spatial

export(PackedScene) var squawk_minigame_scn = null
export(AudioStream) var town_music = null
export(AudioStream) var contest_music = null
export(PackedScene) var duck_scene = null
onready var fade_anim := $Fade/Fade/AnimationPlayer as AnimationPlayer
onready var ducker_iv := $DuckerIVContest as Spatial
onready var lucky := $Lucky as Spatial
onready var squawk_cam := $SquawkCamera as Camera
onready var char_positions := $SquawkMinigameCharPositions as Spatial

var squawk_contest_result := false

func _ready() -> void:
	if squawk_minigame_scn == null:
		printerr("squawk_minigame_scn null at Town.tscn")
	
	if town_music == null:
		printerr("Town music null at Town.tscn")
	else:
		GlobalStatus.play_scene_music(town_music.resource_path, -17)
	
	if GlobalStatus.story_flags_unlocked(["squawk_contest_won"]):
		# Show DuckerIVGuild, hide DuckerIVContest
		$DuckerIVContest.hide()
		$DuckerIVContest/InteractArea.monitorable = false
		$DuckerIVContest/InteractArea.monitoring = false
		$DuckerIVContest/StaticBody/CollisionShape.disabled = true
		$DuckerIVGuild.show()
		$DuckerIVGuild/InteractArea.monitorable = true
		$DuckerIVGuild/InteractArea.monitoring = true
		$DuckerIVGuild/StaticBody/CollisionShape.disabled = false
	else:
		# Show DuckerIVContest, hide DuckerIVGuild
		$DuckerIVContest.show()
		$DuckerIVContest/InteractArea.monitorable = true
		$DuckerIVContest/InteractArea.monitoring = true
		$DuckerIVContest/StaticBody/CollisionShape.disabled = false
		$DuckerIVGuild.hide()
		$DuckerIVGuild/InteractArea.monitorable = false
		$DuckerIVGuild/InteractArea.monitoring = false
		$DuckerIVGuild/StaticBody/CollisionShape.disabled = true
	
	if GlobalStatus.is_demo and GlobalStatus.get_node("DemoTimer").is_stopped():
		GlobalStatus.get_node("DemoTimer").start()
	
	if not GlobalStatus.story_flags_unlocked(["hdtownentry_duckson_1_finished"]):
		GlobalStatus.player.global_transform = Transform(\
			Vector3(0, 0, -1),
			Vector3(0, 1, 0),
			Vector3(1, 0, 0),
			Vector3(-16.397, 0, -4.247))
		$HDTownEntry.talk_look()
	
	if not GlobalStatus.story_flags_unlocked(["postgame_started"]):
		$Duck.hide()
		$Duck/InteractArea.monitorable = false
		$Duck/InteractArea.monitoring = false
		$Duck/StaticBody/CollisionShape.disabled = true
		$MikeBarrel.hide()
	else:
		if GlobalStatus.story_flags_unlocked(["appear_near_statue_on_postgame"]):
			GlobalStatus.remove_story_flag("appear_near_statue_on_postgame")
			GlobalStatus.player.global_transform = Transform(\
				Vector3(1, 0, 0),
				Vector3(0, 1, 0),
				Vector3(0, 0, 1),
				Vector3(40, 0, -3.2))
		$MikeBarrel.show()
		$Model/MikeAnvil.hide()
		$MikeAnvilInteractArea.monitorable = false
		$MikeAnvilInteractArea.monitoring = false


# Returns wether the minigame was completed correctly
func start_squawk_minigame() -> bool:
	# Minigame start
	GUI.action_list.hide()
	GlobalStatus.player.is_on_dialog = true
	fade_anim.play("fade_out")
	GlobalStatus.get_node("AudioStreamPlayer").stop()
	yield(fade_anim, "animation_finished")
	
	# Character setup
	var previous_ducker_iv_xform := ducker_iv.global_transform
	var previous_lucky_xform := lucky.global_transform
	var previous_duckson_xform := GlobalStatus.player.global_transform
	
	ducker_iv.global_transform = char_positions.get_node("DuckerIVPos").global_transform
	var previous_duckeriv_visibility := ducker_iv.visible
	ducker_iv.show()
	lucky.global_transform = char_positions.get_node("LuckyPos").global_transform
	GlobalStatus.player.global_transform = char_positions.get_node("DucksonPos").global_transform
	
	# Minigame instancing
	squawk_cam.current = true
	fade_anim.play("fade_in")
	yield(fade_anim, "animation_finished")
	var scn = squawk_minigame_scn.instance()
	squawk_cam.get_node("AnimationPlayer").play("countdown")
	yield(squawk_cam.get_node("AnimationPlayer"), "animation_finished")
	# Countdown ended
	GUI.add_child(scn)
	squawk_cam.get_node("AnimationPlayer").play("cam_move")
	if contest_music == null:
		printerr("Contest music null at Town.tscn")
	else:
		GlobalStatus.play_scene_music(contest_music.resource_path, -10)
	
	GlobalStatus.player.is_on_dialog = false
	GlobalStatus.player.is_squawking = true
	ducker_iv.anim_player.play("Squawk-loop", -1, 2)
	lucky.anim_player.play("Squawk-loop", -1, 2)
	
	# Minigame finish
	yield(scn, "minigame_finished")
	fade_anim.play("fade_out")
	yield(fade_anim, "animation_finished")
	var res : bool = scn.minigame_result
	scn.queue_free()
	GlobalStatus.player.is_squawking = false
	ducker_iv.global_transform = previous_ducker_iv_xform
	GlobalStatus.player.global_transform = previous_duckson_xform
	lucky.global_transform = previous_lucky_xform
	ducker_iv.anim_player.play("idle-loop")
	ducker_iv.visible = previous_duckeriv_visibility
	lucky.anim_player.play("idle-loop")
	
	if res:
		# Contest won, activate flag, hide DuckerIVContest and show DuckerIVGuild
		GlobalStatus.add_story_flag("squawk_contest_won")
		$DuckerIVContest.hide()
		$DuckerIVContest/InteractArea.monitorable = false
		$DuckerIVContest/InteractArea.monitoring = false
		$DuckerIVContest/StaticBody/CollisionShape.disabled = true
		$DuckerIVGuild.show()
		$DuckerIVGuild/InteractArea.monitorable = true
		$DuckerIVGuild/InteractArea.monitoring = true
		$DuckerIVGuild/StaticBody/CollisionShape.disabled = false
	
	fade_anim.play("fade_in")
	yield(fade_anim, "animation_finished")
	if town_music == null:
		printerr("Town music null at Town.tscn")
	else:
		GlobalStatus.play_scene_music(town_music.resource_path, -17)
	GUI.action_list.show()
	squawk_contest_result = res
	return res