extends Control

onready var anim = get_node("Anim")
signal splash_ended

func _ready():
	set_process_input(true)
	anim.play("fade_in_out")
	yield(anim, "animation_finished")
	emit_signal("splash_ended")
