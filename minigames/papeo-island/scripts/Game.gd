extends Node2D

# Nodes
onready var UI = get_node("UI")
onready var score_counter = UI.get_node("Panel/ScoreCounter")
onready var lives = UI.get_node("Panel/Lives")
onready var ingredients_indicator = UI.get_node("Panel/Ingredients")
onready var recipe_label = UI.get_node("Panel/Preparing")
onready var player = get_node("Player")

# Global vars
var recipes = ["pizza", "burger", "salad", "sandwich", "cake", "hot-dog", "pasta", "omelette", \
	           "sushi", "ramen", "fruit salad", "paella"]
var ingredients = ["cheese", "ham", "lettuce", "tomato", "carrot", "chocolate", "sugar", "bread", "ketchup", "potato", "fish", \
	               "meat", "apple", "lemon", "sausage", "corn", "spaghetti", "rice", "onion", "water", "egg", "banana", \
	               "watermelon"]
var recipe_map = { "pizza":["cheese", "ham", "tomato", "bread"], 
	                   "burger":["meat", "cheese", "bread", "ketchup", "lettuce"],
	                   "salad":["lettuce", "tomato", "corn", "onion", "carrot"],
	                   "sandwich":["bread", "ham", "ketchup", "lettuce", "cheese"],
	                   "cake":["chocolate", "sugar", "bread", "apple"],
	                   "hot-dog":["sausage", "bread", "ketchup", "onion"],
	                   "pasta":["spaghetti", "tomato", "cheese", "meat"],
	                   "omelette":["egg", "potato"],
	                   "sushi":["fish", "rice"],
	                   # i know ramen isn't made with spaghetti, it's just for the sprite
	                   "ramen":["spaghetti", "water", "egg", "onion", "meat"],
	                   "fruit salad":["apple", "banana", "watermelon"],
	                   "paella":["rice", "lemon", "meat", "tomato"]}
var texture_map = load_texture_map()
var current_score = 0
var current_recipe

# positions at which the food can spawn
# 1920 (game width) / 8 = each rail of 240 pixels
var food_rails = [0, 240, 480, 720, 960, 1200, 1440, 1680]

# loading
var food_scn = load("res://minigames/papeo-island/scenes/Food.tscn")

# control food spawning
const SECONDS_BETWEEN_FOOD_SPAWN = 0.4
const INITIAL_SECONDS_BEFORE_SPAWN = 2
var timer = 0
var initial_time_passed = false

signal game_over

func _ready():
	GlobalStatus.play_scene_music("res://assets/minigames/papeo-island/sounds/game_theme.ogg", -10)
	randomize()
	# game pre-setting
	player.alive = true
	#scale_ingredient_sprites(3)
	set_random_recipe()
	# processing
	set_physics_process(true)  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review


func _physics_process(delta):  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
	timer += delta
	if not initial_time_passed and timer > INITIAL_SECONDS_BEFORE_SPAWN:
		initial_time_passed = true
	if timer > SECONDS_BETWEEN_FOOD_SPAWN and initial_time_passed:
		timer = 0
		if player.alive:
			spawn_food()


func load_texture_map() -> Dictionary:
	var res = {}
	for i in ingredients:
		var path = "res://assets/minigames/papeo-island/food/" + i + ".png"
		var texture = load(path)
		if texture != null:
			res[i] = texture
		else:
			print("The " + i + " texture couldn't be loaded!")
	return res


func add_score(score):
	var previous = int(score_counter.get_text())
	current_score = previous + score
	score_counter.set_text("%07d" % current_score)


func remove_life():
	# I know there are better ways to do this but whatever
	if lives.get_node("4").modulate.a > 0:  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
		lives.get_node("4").modulate.a = 0  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
	elif lives.get_node("3").modulate.a > 0:  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
		lives.get_node("3").modulate.a = 0  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
	elif lives.get_node("2").modulate.a > 0:  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
		lives.get_node("2").modulate.a = 0  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
	elif lives.get_node("1").modulate.a > 0:  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
		lives.get_node("1").modulate.a = 0  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
	elif lives.get_node("0").modulate.a > 0:  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
		lives.get_node("0").modulate.a = 0  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
		# game over!
		player.alive = false
		game_over()
	if player.alive:
		GlobalStatus.play_sound("res://assets/minigames/papeo-island/sounds/damage.wav")


func set_random_recipe():
	var next = recipes[randi() % recipes.size()]
	# make sure the recipe doesn't repeat
	while next.to_lower() == current_recipe:
		next = recipes[randi() % recipes.size()]
	recipe_label.set_text("PREPARING " + next.to_upper())
	set_ingredients_indicator(next.to_lower())
	current_recipe = next.to_lower()
	scale_ingredient_sprites(3)


func set_ingredients_indicator(recipe):
	var ingredients_number = recipe_map[recipe].size()
	#make all the ingredients invisible
	for i in ingredients_indicator.get_children():
		i.modulate.a = 0  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
	#make visible only the number of ingredients the recipe has
	for i in range(ingredients_number):
		var sprite = ingredients_indicator.get_child(i)
		var ingredient_name = recipe_map[recipe][i]
		sprite.set_name(ingredient_name)
		sprite.modulate.a = 1  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
		sprite.set_texture(texture_map[ingredient_name])


func spawn_food():
	var food = food_scn.instance()
	self.add_child(food)
	food.set_z_index(-2)
	# set random food
	var current_food_name = ingredients[randi() % ingredients.size()]
	var food_texture = texture_map[current_food_name]
	# change the node name
	food.set_name(current_food_name.to_lower())
	# set the food texture
	food.get_node("Sprite").set_texture(food_texture)
	# scale it
	food.get_node("Sprite").scale = Vector2(3, 3)
	# scale the collision shape accordingly
	var texture_size = food_texture.get_size() * 3*2
	food.set_collision_shape_size(texture_size / 2)
	# add width of the texture to center it
	food.set_position(Vector2(food_rails[randi() % food_rails.size()] + (texture_size.x), 0))  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review


func process_collected_food(food_name):
	var contains_collected = false
	var ingredient_collected = null
	for ingredient in recipe_map[current_recipe]:
		if food_name.findn(ingredient) != -1:
			contains_collected = true
			ingredient_collected = ingredient
			break
		else:
			contains_collected = false
	if contains_collected:
		# the collected food is in the recipe
		for i in ingredients_indicator.get_children():
			if i.get_name().findn(ingredient_collected) != -1:
				GlobalStatus.play_sound("res://assets/minigames/papeo-island/sounds/ingredient_pick.wav")
				i.modulate.a = 0  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
				add_score(100)
				check_recipe_is_completed()
				break
	else:
		remove_life()


func check_recipe_is_completed():
	var is_completed = false
	for i in range(ingredients_indicator.get_children().size()):
		if ingredients_indicator.get_child(i).modulate.a == 0:  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
			is_completed = true
		else:
			is_completed = false
			break
	if is_completed:
		add_score(recipe_map[current_recipe].size() * 100)
		set_random_recipe()


func game_over():
	player.die() # rip
	# stop game sound
	GlobalStatus.stop_all_sounds()
	if int(score_counter.text) > GlobalStatus.papeo_island_high_score:
		GlobalStatus.papeo_island_high_score = int(score_counter.text)
	# death sound
	GlobalStatus.play_sound("res://assets/minigames/papeo-island/sounds/death.wav")
	UI.get_node("Anim").play("move_in_gameoverpanel")
	yield(UI.get_node("Anim"), "animation_finished")
	emit_signal("game_over")


func scale_ingredient_sprites(factor):
	for i in ingredients_indicator.get_children():
		i.scale = Vector2(factor, factor)
