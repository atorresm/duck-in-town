extends Control

export (PackedScene) var next_scene

func _ready():
	# display splash screen
	var next_scene_instance = next_scene.instance()
	add_child(next_scene_instance)
	next_scene_instance.is_loading = false