extends Node

# The purpose of this scene is to emulate the "Globals" singleton
# in Godot 2.x

var info := {}
const sounds = {
	"damage" : "res://assets/sounds/damage.wav",
	"death" : "res://assets/sounds/death.wav",
	"game_theme" : "res://assets/sounds/game_theme.ogg",
	"ingredient_pick" : "res://assets/sounds/ingredient_pick.wav",
	"main_theme" : "res://assets/sounds/main_theme.ogg",
	"menu_move" : "res://assets/sounds/menu_move.wav",
	"menu_select" : "res://assets/sounds/menu_select.wav"
}


func set(key : String, value) -> void:
	info[key] = value
 

func get(key : String):
	if info.has(key):
		return info[key]
	else:
		return null


func play_sound(id : String) -> void:
	if $AudioPlayer.playing:
		$AudioPlayer.stop()
	$AudioPlayer.stream = load(sounds[id])
	$AudioPlayer.play()


func stop_sound() -> void:
	$AudioPlayer.stop()
