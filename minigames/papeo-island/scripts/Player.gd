extends KinematicBody2D

var input_direction = 0
var direction = 0
var velocity = 0
const SPEED = 800
var alive = true
onready var anim = get_node("Anim")

func _ready():
	set_physics_process(true)  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review

func _physics_process(delta):  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
	var pos = get_position()  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
	set_position(bound_position(pos))  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
	
	if input_direction != null:
		direction = input_direction
	if Input.is_action_pressed("papeo_island_left"):
		input_direction = -1
	elif Input.is_action_pressed("papeo_island_right"):
		input_direction = 1
	else:
		input_direction = 0

	#movement
	velocity = SPEED * delta * direction
	if alive:
# warning-ignore:return_value_discarded
		move_and_collide(Vector2(velocity, 0))  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review

func bound_position(pos):  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
	var sprite_size_offset = (get_node("Sprite").get_texture().get_size().x / 2) * self.scale.x
	if pos.x > 1920 - sprite_size_offset:
		pos.x = 1920 - sprite_size_offset
	if pos.x < 0 + sprite_size_offset:
		pos.x = 0 + sprite_size_offset
	return pos

func _on_Area2D_body_enter(body):
	if alive:
		get_node("..").process_collected_food(body.get_name())
	body.queue_free()

func die():
	anim.play("die")
