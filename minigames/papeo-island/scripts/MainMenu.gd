extends Control

# pointer of the menus
var index = 0
onready var pointer = get_node("HUD").get_node("Entries").get_node("Pointer")
# animation player
onready var anim = get_node("Anim")
# flags
var credits_showing = false

signal game_started
signal game_exited

func _ready():
	GlobalStatus.play_scene_music("res://assets/minigames/papeo-island/sounds/main_theme.ogg", -10)
	set_process_input(true)
	set_physics_process(true)  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
	# hide the mouse
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	$HighScore.text = "HIGH SCORE: " + ("%07d" % GlobalStatus.papeo_island_high_score)

func _input(event):
	var is_hud_anim_finished = not anim.is_playing()
	if event.is_action_pressed("papeo_island_up") and not event.is_echo() and not credits_showing and is_hud_anim_finished:
		GlobalStatus.play_sound("res://assets/minigames/papeo-island/sounds/menu_move.wav")
		if index != 0:
			# decrease index
			index -= 1
			pointer.set_position(Vector2(pointer.get_position().x, pointer.get_position().y - 30*2))  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
		else:
			index = 2
			pointer.set_position(Vector2(pointer.get_position().x, pointer.get_position().y + 60*2))  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
	if event.is_action_pressed("papeo_island_down") and not event.is_echo() and not credits_showing and is_hud_anim_finished:
		GlobalStatus.play_sound("res://assets/minigames/papeo-island/sounds/menu_move.wav")
		if index != 2:
			index += 1
			pointer.set_position(Vector2(pointer.get_position().x, pointer.get_position().y + 30*2))  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
		else:
			index = 0
			pointer.set_position(Vector2(pointer.get_position().x, pointer.get_position().y - 60*2))  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
	if event.is_action_pressed("ui_accept") and not event.is_echo() and is_hud_anim_finished:
		if credits_showing:
			anim.play("credits_hide")
			credits_showing = false
		elif index == 0:
			GlobalStatus.play_sound("res://assets/minigames/papeo-island/sounds/menu_select.wav")
			# start new game
			anim.play("pointer_blink")
			yield(anim, "animation_finished")
			load_new_game()
		elif index == 1:
			GlobalStatus.play_sound("res://assets/minigames/papeo-island/sounds/menu_select.wav")
			# show credits
			anim.play("pointer_blink")
			yield(anim, "animation_finished")
			anim.play("credits_show")
			credits_showing = true
		elif index == 2:
			GlobalStatus.play_sound("res://assets/minigames/papeo-island/sounds/menu_select.wav")
			# quit game
			anim.play("pointer_blink")
			yield(anim, "animation_finished")
			emit_signal("game_exited")


func load_new_game():
	emit_signal("game_started")
