extends KinematicBody2D

var SPEED = 400
var velocity = 0
var direction = Vector2(0, 1)

func _ready():
	set_physics_process(true)  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review

func _physics_process(delta):  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
	SPEED += delta * 2
	if SPEED > 600:
		SPEED = 600
	velocity = SPEED * delta * direction
# warning-ignore:return_value_discarded
	move_and_collide(velocity)  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
	if get_position().y > 1080 - ((get_node("Sprite").get_texture().get_size().y / 2) * self.scale.x):  #-- NOTE: Automatically converted by Godot 2 to 3 converter, please review
		self.queue_free()

func set_collision_shape_size(size):
	var collision = CollisionShape2D.new()
	collision.set_shape(RectangleShape2D.new())
	self.add_child(collision)
	var xform = collision.get_shape()
	xform.set_extents(size)
