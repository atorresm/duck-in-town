extends Node

# NOTE: Edit settings in settings.ini, not here.
const SETTINGS_PATH = "user://settings.ini"
var config_file := ConfigFile.new()


# Directional shadows quality
# 0: no shadows
# 1: medium quality shadows
# 2: high quality shadows
var directional_shadows_quality := 2

# MSAA
# 0: Disabled
# 1: x2
# 2: x4
# 3: x8
# 4: x16
var msaa := 1

# V-Sync
# False: disabled
# True: Enabled
var vsync := true

# Fullscreen mode
var fullscreen := true


var locale := ""


func _ready() -> void:
	var ok = _parse_settings_file()
	if not ok:
		printerr("Loading default settings.")
		load_default_settings()
	ok = save_settings_to_file()
	if ok:
		apply_settings()
	else:
		printerr("Error saving settings to config file.")


func _parse_settings_file() -> bool:
	var err = config_file.load(SETTINGS_PATH)
	if err != OK:
		printerr("Error loading settings.ini file.")
		return false
	
	# ----------
	# [graphics]
	# ----------
	
	# Directional shadows quality
	var dsq_val = config_file.get_value("graphics", "directional_shadows_quality", 2)
# warning-ignore:narrowing_conversion
	self.directional_shadows_quality = clamp(int(dsq_val), 0, 2)
	
	# MSAA
	var msaa_val = config_file.get_value("graphics", "msaa", 2)
# warning-ignore:narrowing_conversion
	self.msaa = clamp(int(msaa_val), 0, 4)
	
	# Vsync
	var vsync_val = config_file.get_value("graphics", "vsync", true)
	self.vsync = bool(vsync_val)
	
	# Fullscreen mode
	var fullscreen_val = config_file.get_value("graphics", "fullscreen", false)
	self.fullscreen = bool(fullscreen_val)
	
	# Translation
	var locale_val = config_file.get_value("user", "locale", TranslationServer.get_locale())
	self.locale = str(locale_val)
	
	return true


func apply_settings() -> void:
	# Directional shadows quality
	_apply_directional_shadows_settings()
	
	# MSAA
	ProjectSettings.set_setting("rendering/quality/filters/msaa", self.msaa)
	get_viewport().msaa = self.msaa
	
	# Vsync
	OS.vsync_enabled = self.vsync
	
	# Fullscreen model
	OS.window_fullscreen = self.fullscreen
	
	# Translation
	TranslationServer.set_locale(locale)
	
	save_settings_to_file()


func save_settings_to_file() -> bool:
	# graphics
	config_file.set_value("graphics", "directional_shadows_quality", self.directional_shadows_quality)
	config_file.set_value("graphics", "msaa", self.msaa)
	config_file.set_value("graphics", "vsync", self.vsync)
	config_file.set_value("graphics", "fullscreen", self.fullscreen)
	config_file.set_value("user", "locale", self.locale)

	return config_file.save(SETTINGS_PATH) == OK


func load_default_settings() -> void:
	var defs = get_default_settings()
	for key in defs.keys():
		self.set(key, defs[key])


func get_default_settings() -> Dictionary:
	return {
		"directional_shadows_quality" : 2,
		"msaa" : 1,
		"vsync" : true,
		"fullscreen" : true,
		"locale" : OS.get_locale()
	}


func _apply_directional_shadows_settings() -> void:
	var size : int
	var filter_mode : int
	match self.directional_shadows_quality:
		0, 1:
			size = 4096
			# PCF5
			filter_mode = 1
		2:
			size = 8192
			# PCF13
			filter_mode = 2
	ProjectSettings.set_setting("rendering/quality/directional_shadow/size", size)
	ProjectSettings.set_setting("rendering/quality/shadows/filter_mode", filter_mode)