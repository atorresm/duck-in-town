extends Control

onready var label := $Label as Label
var text : String setget _set_text, _get_text
signal pressed


func _input(event: InputEvent) -> void:
	if has_focus() and event.is_action_released("ui_accept"):
		emit_signal("pressed")


func _on_DialogOptionEntry_focus_entered() -> void:
	label.self_modulate = Color("f22f2f")


func _on_DialogOptionEntry_focus_exited() -> void:
	label.self_modulate = Color("ffffff")


func _set_text(t : String) -> void:
	text = t
	label.text = t


func _get_text() -> String:
	return text