extends Object

class_name YarnNode

var id : String = ""
var tags : PoolStringArray = PoolStringArray()
# Array of YarnLines
var lines : Array = []
var character_name := "" # Name of the character saying the current node
