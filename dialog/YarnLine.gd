extends Object

class_name YarnLine

class Type:
	const TEXT = "text"
	const CHOICE = "choice"
	const CHARACTER_NAME = "character_name"
	const COMMAND = "command"
	const REDIRECT = "redirect"


var type := ""
var text := ""
var next_node_id := ""